Readme
------
There's a bug in drupal that causes comment links to go to the wrong node page on occasion. If you're not familiar with the issue, the discussion is at http://drupal.org/node/26966. 

As of this writing, the bug has been pushed off to drupal 7. I needed something sooner, so I took one of the patches (http://drupal.org/node/26966#comment-335956) and made it into a module. I'll be very happy when I can deprecate this module b/c it'll mean the bug'll finally be fixed.

Send comments to Gwen Park at: http://drupal.org/user/99925.

Requirements
------------
This module requires Drupal 5.x and the comment module. If you want the views tweaks, this module requires at least version 1.6.

Installation
------------
1. Copy the comment_redirect directory and all its contents to the Drupal modules directory, probably /sites/all/modules/.

2. Enable the Comment Redirect module on the modules admin page /admin/build/modules.

If you want to override the link in the actual comment, add an override theme function in yourmodule for theme_comment. Something like:

function yourmodule_comment($comment, $links = array()) {
  $output  = '<div class="comment'. ($comment->status == COMMENT_NOT_PUBLISHED ? ' comment-unpublished' : '') .'">';
  $output .= '<div class="subject">'. l($comment->subject, 'comment_redirect/' . $comment->cid) .' '. theme('mark', $comment->new) ."</div>\n";
  $output .= '<div class="credit">'. t('by %a on %b', array('%a' => theme('username', $comment), '%b' => format_date($comment->timestamp))) ."</div>\n";
  $output .= '<div class="body">'. $comment->comment .'</div>';
  $output .= '<div class="links">'. theme('links', $links) .'</div>';
  $output .= '</div>';
  return $output;
}

Tracker Module
--------------

The tracker module has "new" links that run into this same problem. B/c of how the code is written, this module does not fix that instance. If you want to fix that problem with this module, you'll need to do some edits to the tracker module function tracker_page(). No code for that right now, it's on the todo list.

Todo
----
1. upgrade to drupal 6
2. add a patch that can be applied to the tracker module

Credits
-------
Special thanks to Sung-Suh Park (no relation) for providing a patch for the bug that I built on top of.

Written by Gwen Park.
